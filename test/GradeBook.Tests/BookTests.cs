using System;
using Xunit;

namespace GradeBook.Tests
{
    public class BookTests
    {
        [Fact]
        public void BookCalculatesAnAverageGrade()
        {
            //arrange
            var book = new Book("");
            book.AddGrade(89.1);
            book.AddGrade(99.1);
            book.AddGrade(79.4);

            //act
            var result = book.GetStatistics();   

            //assert
            Assert.Equal(89.2, result.Average);
            Assert.Equal(99.1, result.High);
            Assert.Equal(79.4, result.Low);
        }
    }
}
